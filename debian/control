Source: golang-github-muesli-sasquatch
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-mitchellh-go-homedir-dev (>= 1.1.0),
               golang-golang-x-crypto-dev (>= 1:0.0~git20200728.123391f),
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-muesli-sasquatch
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-muesli-sasquatch.git
Homepage: https://github.com/muesli/sasquatch
XS-Go-Import-Path: github.com/muesli/sasquatch

Package: golang-github-muesli-sasquatch-dev
Architecture: all
Multi-Arch: foreign
Depends: golang-github-mitchellh-go-homedir-dev (>= 1.1.0),
         golang-golang-x-crypto-dev (>= 1:0.0~git20200728.123391f),
         ${misc:Depends}
Description: simple data encryption library for Go
 A simple data encryption library, heavily inspired by @Benjojo12 and
 @FiloSottile's fantastic "age" project.
 .
 Features:
 .
  * Multiple recipients
  * Supports encrypting with your existing SSH keys / ssh-agent
  * Convenient API
 .
 Crypto Backends:
 .
  * ssh-rsa
  * ssh-ed25519
  * ssh-agent signing challenge (excluding ECDSA identities, as ECDSA
    signatures aren't deterministic)
  * scrypt / password
